import telebot
import requests
import json
import datetime
import google_connect

# ART = int(input('Введи артикул:'))

TOKEN = "266765795:AAGvkYUISR8Ete2phdFyD6MA_RqFWzPcYbo"
bot = telebot.TeleBot(TOKEN)
chat_id = '167966841'


def returning_feedbacks(art: int, identificator):
    '''
    Функция возвращающая список id комментариев по заданному артикулу
    Считает количество комментариев и если их > 999, то приравнивает их к 999
    Далее, составляет список из id коммента и возвращает его
    :param art: int
    :return: list
    '''
    print(art)
    main_url = "https://www.wildberries.ru/catalog/{ART}/detail.aspx".format(ART=art)

    url = "https://card.wb.ru/cards/detail?spp=0&regions=80,64,38,4,83,33,68,70,69,30,86,75,40,1,22,66,31,48,71&" \
          "pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=" \
          "12,3,18,15,21&dest=-1257786&nm={art}".format(art=art)  # 5163995;6170054;31211424"

    res = requests.get(url, headers={
        "accept": "*/*",
        "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site"
    })

    res_json = json.loads(res.text)
    # print(res_json)
    # print(res_json['data']['products'][0]['root'])

    rootID = res_json['data']['products'][0]['root']

    url2 = "https://feedbacks1.wb.ru/feedbacks/v1/{rootID}".format(rootID=rootID)
    url2_reserved = "https://feedbacks2.wb.ru/feedbacks/v1/{rootID}".format(rootID=rootID)

    res_feedback = requests.get(url2, headers={
        "accept": "*/*",
        "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site"
    })

    feedback_json = json.loads(res_feedback.text)
    # print('Всего отзывов:', feedback_json['feedbackCount'])

    if feedback_json['feedbackCount'] == 0:
        # print('пересчет')
        res_feedback = requests.get(url2_reserved, headers={
        "accept": "*/*",
        "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site"
        })

    feedback_json = json.loads(res_feedback.text)
    print('Всего отзывов:', feedback_json['feedbackCount'])

    id_list = []

    feedbacks = 999 if feedback_json['feedbackCount'] > 999 else feedback_json['feedbackCount']

    for i in range(feedbacks):  # range(feedback_json['feedbackCount']):
        # print(feedback_json['feedbacks'][i]['id'])
        if feedback_json['feedbacks'][i]['createdDate'].split('T')[0] == str(datetime.datetime.now().date()):
            # id_list.append(feedback_json['feedbacks'][i]['id'])
            if not google_connect.checking_feedback(feedback_json['feedbacks'][i]['id']) and \
                    feedback_json['feedbacks'][i]['productValuation'] < 4:
                google_connect.adding_feedback_id(feedback_json['feedbacks'][i]['id'])
                string_warn = 'Отрицательный отзыв на ' + '[{product}]({url})'.format(product=identificator, url=main_url) \
                                              + '\nОценка:{mark}'.format(mark=feedback_json['feedbacks'][i]['productValuation']*u"\u2B50")
                bot.send_message(chat_id=chat_id, text=string_warn, parse_mode='Markdown')

        # if feedback_json['feedbacks'][i]['id'] not in id_list:
        #     id_list.append(feedback_json['feedbacks'][i]['id'])
        #     print(feedback_json['feedbacks'][i]['createdDate'].split('T')[0], '--->', datetime.datetime.now().date())
        #     if feedback_json['feedbacks'][i]['createdDate'].split('T')[0] == str(datetime.datetime.now().date()):
        #         print('today comment')
        #         if feedback_json['feedbacks'][i]['productValuation'] < 4:
        #             string_warn = 'Отрицательный отзыв на ' + '[{product}]({url})'.format(product='что-то', url=main_url) \
        #                           + '\nОценка:{mark}'.format(mark=feedback_json['feedbacks'][i]['productValuation']*u"\u2B50")
        #             bot.send_message(chat_id=chat_id, text=string_warn, parse_mode='Markdown')
    # print(id_list)
    # return id_list


# returning_feedbacks(art=ART)
