import gspread
import sqlite3
import telebot
import requests
import json
import datetime
import configparser

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/wb/Rodion/config")
print(config['telegram']['channel_id'])
print(config['telegram']['TOKEN_2'])

channel_id = config['telegram']['channel_id']
TOKEN_2 = config['telegram']['TOKEN_2']
bot = telebot.TeleBot(TOKEN_2)


# TOKEN = "266765795:AAGvkYUISR8Ete2phdFyD6MA_RqFWzPcYbo"
# bot = telebot.TeleBot(TOKEN)
# chat_id = '167966841'

conn = sqlite3.connect('/etc/bot/andrew/wb/db/database.db', check_same_thread=False)
#conn = sqlite3.connect('../db/database.db', check_same_thread=False)
cursor = conn.cursor()


cursor.execute('''
            CREATE TABLE IF NOT EXISTS feedbacks_rodion
            ([feedback_id] VARCHAR)
            ''')

'''
Подключаемся к google аккаунту, берем информацию таблицы
Далее указаны id листов, которые нам нужны
'''

gs = gspread.service_account(filename='/etc/bot/ostatok/big-elysium-364512-847480f2e88d.json')
sh = gs.open_by_key('1YYcNMC22fh5kSVpmHTZeow0KDjvDvGxLHWFVdvoX2D0')
# id_sheet = 1651709231
# worksheet = sh.get_worksheet_by_id(id_sheet)  # если запрос будет к листу
worksheet = sh.sheet1


def adding_feedback_id(feedback_id):
    cursor.execute('INSERT INTO feedbacks_rodion (feedback_id) VALUES (?)', (feedback_id,))
    conn.commit()


def checking_feedback(id):
    result = cursor.execute('SELECT EXISTS (SELECT feedback_id FROM feedbacks_rodion WHERE feedback_id="{id}")'.format(id=id))
    conn.commit()
    final = result.fetchone()[0]
    return final


#def checking_art(art):
#    result = cursor.execute('SELECT EXISTS (SELECT articule FROM products_vitaliy WHERE articule={art})'.format(art=art))
#    conn.commit()
#    return result.fetchone()[0]


def request():
    art_range = cursor.execute('SELECT articule FROM products_vitaliy')
    conn.commit()
    return art_range


def working_with_art(id_list: int) -> None:
    """
    Функция получает список id комментариев по каждому артикулу
    Комментарии берутся из функции feedback.returning_feedbacks
    :param id_list: id конкретного листа в таблице
    :return:
    """

    # worksheet = sh.get_worksheet_by_id(id_list)
    # my_list = worksheet.get_all_records()
    my_list = sh.get_worksheet_by_id(id_list).get_all_records()
    print(my_list)
    for element in my_list:
        print(element['Артикул'], element['Идентификатор'])
        returning_feedbacks(element['Артикул'], element['Идентификатор'])


def returning_feedbacks(art: int, identificator):
    """
    Функция возвращающая список id комментариев по заданному артикулу
    Считает количество комментариев и если их > 999, то приравнивает их к 999
    Далее, составляет список из id коммента и возвращает его
    :param identificator: product name
    :param art: int product articule
    :return: list
    """
    print(art)
    main_url = "https://www.wildberries.ru/catalog/{ART}/detail.aspx".format(ART=art)

    url = "https://card.wb.ru/cards/detail?spp=0&regions=80,64,38,4,83,33,68,70,69,30,86,75,40,1,22,66,31,48,71&" \
          "pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=" \
          "12,3,18,15,21&dest=-1257786&nm={art}".format(art=art)  # 5163995;6170054;31211424"

    res = requests.get(url, headers={
        "accept": "*/*",
        "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site"
    })

    res_json = json.loads(res.text)

    try:
        rootID = res_json['data']['products'][0]['root']

        url2 = "https://feedbacks1.wb.ru/feedbacks/v1/{rootID}".format(rootID=rootID)
        url2_reserved = "https://feedbacks2.wb.ru/feedbacks/v1/{rootID}".format(rootID=rootID)

        res_feedback = requests.get(url2, headers={
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site"
        })

        feedback_json = json.loads(res_feedback.text)

        if feedback_json['feedbackCount'] == 0:
            # print('пересчет')
            res_feedback = requests.get(url2_reserved, headers={
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site"
            })

        feedback_json = json.loads(res_feedback.text)
        print('Всего отзывов:', feedback_json['feedbackCount'])

        feedbacks = 999 if feedback_json['feedbackCount'] > 999 else feedback_json['feedbackCount']

        for i in range(feedbacks):  # range(feedback_json['feedbackCount']):
            # print(feedback_json['feedbacks'][i]['id'])
            if feedback_json['feedbacks'][i]['createdDate'].split('T')[0] == str(datetime.datetime.now().date()):
                # id_list.append(feedback_json['feedbacks'][i]['id'])
                if not checking_feedback(feedback_json['feedbacks'][i]['id']) and \
                        feedback_json['feedbacks'][i]['productValuation'] < 4:
                    adding_feedback_id(feedback_json['feedbacks'][i]['id'])
                    string_warn = 'На ' + '[{product}]({url})'.format(product=identificator, url=main_url) \
                                  + ' негативчик' + '\nОценка: {mark}'.format(
                        mark=feedback_json['feedbacks'][i]['productValuation'])  # u"\u2B50"
                    bot.send_message(chat_id=channel_id, text=string_warn, parse_mode='Markdown')
    except:
        print('Странно', identificator, main_url)


working_with_art(0)
