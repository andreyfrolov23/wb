import gspread
import sqlite3
import telebot
import time
import configparser

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/wb/danya_ostatok/config")
print(config['telegram']['channel_id'])
print(config['telegram']['TOKEN_2'])

TOKEN_2 = config['telegram']['TOKEN_2']
bot_2 = telebot.TeleBot(TOKEN_2)
channel_id = config['telegram']['channel_id']

conn = sqlite3.connect('/etc/bot/andrew/wb/db/database.db', check_same_thread=False)
cursor = conn.cursor()
cursor.execute('''
            CREATE TABLE IF NOT EXISTS ostatok_danya
            ([articule] INTEGER UNIQUE NOT NULL, [ost] INT)
            ''')

'''
Подключаемся к google аккаунту, берем информацию таблицы
Далее указаны id листов, которые нам нужны
'''
# gs = gspread.service_account(filename='../credits.json')
# sh = gs.open_by_key('1D4qv3HTDAIxVpKvb2VckDBVHoRmbxzJPjAAMZEcQjkk')
gs = gspread.service_account(filename='/etc/bot/ostatok/big-elysium-364512-847480f2e88d.json')
sh = gs.open_by_key('1sLnDs-1h18r_DXh1Ka0Z2rFcDVVbKysYfE0f84V9sLY')
# id_sheet = 1651709231
# worksheet = sh.get_worksheet_by_id(id_sheet)  # если запрос будет к листу
worksheet = sh.sheet1


def adding_info(art: int, ost: int) -> None:
    cursor.execute('INSERT INTO ostatok_danya (articule, ost) VALUES (?, ?)', (art, ost))
    conn.commit()


def request():
    art_range = cursor.execute('SELECT articule FROM ostatok_danya')
    conn.commit()
    return art_range


def request_ostatok(my_art: int) -> int:
    my_ostatok = cursor.execute('SELECT ost FROM ostatok_danya WHERE articule={my_art}'
                                .format(my_art=my_art))
    conn.commit()
    return my_ostatok.fetchone()[0]


def update_table(my_art: int, ost: int) -> None:
    cursor.execute(f'UPDATE ostatok_danya SET ost={ost} WHERE articule={my_art}')
    conn.commit()


def main_func():
    data_ost = worksheet.get_all_records()
    for i in range(len(data_ost)):
        my_art_range = request()
        list_art = []
        for row in my_art_range.fetchall():
            list_art.append(row[0])
        if data_ost[i]['Артикул'] in list_art:
            print('уже есть!!!')
            my_ost = request_ostatok(data_ost[i]['Артикул'])
            if data_ost[i]['Остаток'] == 0 and data_ost[i]['Остаток'] != my_ost:
                print('Danger')
                update_table(data_ost[i]['Артикул'], data_ost[i]['Остаток'])
                text = 'Остаток ' \
                       + '[{product}](https://www.wildberries.ru/catalog/{art}/detail.aspx)'\
                           .format(product=data_ost[i]['Идентификатор'], art=data_ost[i]['Артикул']) \
                       + ' стал равен нулю.'
                bot_2.send_message(chat_id=channel_id, text=text, parse_mode='Markdown')
            elif data_ost[i]['Остаток'] != 0 and my_ost == 0:
                print('Добавили', data_ost[i]['Остаток'], my_ost)
                update_table(data_ost[i]['Артикул'], data_ost[i]['Остаток'])
                text2 = 'Остаток ' + '[{product}](https://www.wildberries.ru/catalog/{art}/detail.aspx)'\
                    .format(product=data_ost[i]['Идентификатор'], art=data_ost[i]['Артикул'])\
                        + ' поменялся с нуля на {ost}'.format(ost=data_ost[i]['Остаток'])
                bot_2.send_message(chat_id=channel_id, text=text2, parse_mode='Markdown')
        else:
            print('Новый артикул!!!', data_ost[i]['Артикул'], '->', data_ost[i]['Остаток'])
            adding_info(data_ost[i]['Артикул'], data_ost[i]['Остаток'])


if __name__ == '__main__':
    main_func()
