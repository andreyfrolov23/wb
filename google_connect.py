import gspread
import sqlite3
import telebot
import requests
import json
import datetime
import configparser

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/wb/config_feedbacks")
print(config['telegram']['channel_id'])
print(config['telegram']['TOKEN_2'])

channel_id = config['telegram']['channel_id']
TOKEN_2 = config['telegram']['TOKEN_2']
bot = telebot.TeleBot(TOKEN_2)


# TOKEN = "266765795:AAGvkYUISR8Ete2phdFyD6MA_RqFWzPcYbo"
# bot = telebot.TeleBot(TOKEN)
# chat_id = '167966841'

conn = sqlite3.connect('/etc/bot/andrew/wb/db/database.db', check_same_thread=False)
cursor = conn.cursor()
cursor.execute('''
            CREATE TABLE IF NOT EXISTS products_vitaliy
            ([articule] INTEGER UNIQUE NOT NULL, [identity] VARCHAR(50))
            ''')

cursor.execute('''
            CREATE TABLE IF NOT EXISTS feedbacks_vitaliy
            ([feedback_id] VARCHAR)
            ''')

'''
Подключаемся к google аккаунту, берем информацию таблицы
Далее указаны id листов, которые нам нужны
'''
gs = gspread.service_account(filename='/etc/bot/ostatok/big-elysium-364512-847480f2e88d.json')
# sh = gs.open_by_key('1D4qv3HTDAIxVpKvb2VckDBVHoRmbxzJPjAAMZEcQjkk')
sh = gs.open_by_key('1wDt5pRJ6wnsMIuy3beoKDAQmmp1ur36YaWzeweiKE5o')
worksheet = sh.sheet1

# data = worksheet.get_all_records()
# data = worksheet.col_values(1)
# data = sh.get_worksheet_by_id(0).col_values(1)
# data2 = sh.get_worksheet_by_id(id_sheet2).col_values(1)
# data3 = sh.get_worksheet_by_id(id_sheet3).col_values(1)

# TODO доделать создание таблиц с артикулами и списками комментов для них


def adding_info(art, identity):
    cursor.execute('INSERT INTO products_vitaliy (articule, identity) VALUES (?, ?)', (art, identity))
    conn.commit()


def adding_feedback_id(feedback_id):
    cursor.execute('INSERT INTO feedbacks_vitaliy (feedback_id) VALUES (?)', (feedback_id,))
    conn.commit()


def checking_feedback(id):
    result = cursor.execute('SELECT EXISTS (SELECT feedback_id FROM feedbacks_vitaliy WHERE feedback_id="{id}")'.format(id=id))
    conn.commit()
    final = result.fetchone()[0]
    return final


def checking_art(art):
    result = cursor.execute('SELECT EXISTS (SELECT articule FROM products_vitaliy WHERE articule={art})'.format(art=art))
    conn.commit()
    return result.fetchone()[0]


def request():
    art_range = cursor.execute('SELECT articule FROM products_vitaliy')
    conn.commit()
    return art_range


def working_with_art(id_list: int) -> None:
    """
    Функция получает список id комментариев по каждому артикулу
    Комментарии берутся из функции feedback.returning_feedbacks
    :param id_list: id конкретного листа в таблице
    :return:
    """
    my_list = sh.get_worksheet_by_id(id_list).get_all_records()
    # my_list = worksheet.get_all_records()
    print(my_list)
    # my_art_range = request()
    # for i in my_art_range:
    #     print(i[0])
    for element in my_list:
        if not checking_art(element['Артикул']):
            adding_info(element['Артикул'], element['Идентификатор'])
        print(element['Артикул'], element['Идентификатор'])
        returning_feedbacks(element['Артикул'], element['Идентификатор'])
        # for feedback_id in list_feedback_id:
        #     if not checking_feedback(feedback_id):
        #         adding_feedback_id(feedback_id)

    # for art in my_list[1:]:
    #     list_feedback_id = feedback.returning_feedbacks(art)
    #     print(art, '->', list_feedback_id)
    # print(my_list[1:])


def returning_feedbacks(art: int, identificator):
    """
    Функция возвращающая список id комментариев по заданному артикулу
    Считает количество комментариев и если их > 999, то приравнивает их к 999
    Далее, составляет список из id коммента и возвращает его
    :param identificator: product name
    :param art: int product articule
    :return: list
    """
    print(art)
    main_url = "https://www.wildberries.ru/catalog/{ART}/detail.aspx".format(ART=art)

    url = "https://card.wb.ru/cards/detail?spp=0&regions=80,64,38,4,83,33,68,70,69,30,86,75,40,1,22,66,31,48,71&" \
          "pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=" \
          "12,3,18,15,21&dest=-1257786&nm={art}".format(art=art)  # 5163995;6170054;31211424"

    res = requests.get(url, headers={
        "accept": "*/*",
        "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": "\"Windows\"",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "cross-site"
    })

    res_json = json.loads(res.text)
    # print(res_json)
    # print(res_json['data']['products'][0]['root'])

    try:
        rootID = res_json['data']['products'][0]['root']

        url2 = "https://feedbacks1.wb.ru/feedbacks/v1/{rootID}".format(rootID=rootID)
        url2_reserved = "https://feedbacks2.wb.ru/feedbacks/v1/{rootID}".format(rootID=rootID)

        res_feedback = requests.get(url2, headers={
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site"
        })

        feedback_json = json.loads(res_feedback.text)
        # print('Всего отзывов:', feedback_json['feedbackCount'])

        if feedback_json['feedbackCount'] == 0:
            # print('пересчет')
            res_feedback = requests.get(url2_reserved, headers={
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site"
            })

        feedback_json = json.loads(res_feedback.text)
        print('Всего отзывов:', feedback_json['feedbackCount'])

        id_list = []

        feedbacks = 999 if feedback_json['feedbackCount'] > 999 else feedback_json['feedbackCount']

        for i in range(feedbacks):  # range(feedback_json['feedbackCount']):
            # print(feedback_json['feedbacks'][i]['id'])
            if feedback_json['feedbacks'][i]['createdDate'].split('T')[0] == str(datetime.datetime.now().date()):
                # id_list.append(feedback_json['feedbacks'][i]['id'])
                if not checking_feedback(feedback_json['feedbacks'][i]['id']) and \
                        feedback_json['feedbacks'][i]['productValuation'] < 4:
                    adding_feedback_id(feedback_json['feedbacks'][i]['id'])
                    string_warn = 'На ' + '[{product}]({url})'.format(product=identificator, url=main_url) \
                                  + ' негативчик' + '\nОценка: {mark}'.format(
                        mark=feedback_json['feedbacks'][i]['productValuation'])  # u"\u2B50"
                    bot.send_message(chat_id=channel_id, text=string_warn, parse_mode='Markdown')
    except:
        print('Странно', identificator, main_url, checking_art(art))
        # if not checking_art(art):
        #     text_str = "Странный товар" + '[{product}]({url})'.format(product=identificator, url=main_url)
        #     bot.send_message(chat_id=channel_id, text=text_str, parse_mode='Markdown')


# print(sh.get_worksheet_by_id(0).get_all_records())
working_with_art(0)
# working_with_art(id_sheet2)
# working_with_art(id_sheet3)

